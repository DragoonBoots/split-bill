# Split Bill
[![pipeline status](https://gitlab.com/DragoonBoots/split-bill/badges/master/pipeline.svg)](https://gitlab.com/DragoonBoots/split-bill/-/commits/master)

## Download
See downloads available [here](https://dragoonboots.gitlab.io/split-bill/download.html).

## License
Licensed under the [GNU General Public License, Version 3](https://www.gnu.org/licenses/gpl-3.0.html).
